﻿using System;
using System.Collections.Generic;
using System.Linq;

using Autodesk.Revit.DB;

namespace RevitGeometryUtilities.Geometry
{
	/// <summary>
	/// A collection of Geometry helper methods to extend the Revit API for creating and analysing Revit Geometry.
	/// </summary>
    public static class GeometryUtilities
    {

		/// <summary>
		/// Calculates the Determinant of a 3x3 Matrix.
		/// </summary>
		/// <param name="v1">First Vector3 as XYZ</param>
		/// <param name="v2">Second Vector3 as XYZ</param>
		/// <param name="v3">Third Vector3 as XYZ</param>
		/// <returns>Determinant value as <see cref="Double"/>.</returns>
		public static double Determinant3x3(XYZ v1, XYZ v2, XYZ v3)
		{
			// TODO: Probably create a Math utility class for the Determinant3x3 that takes a triple of Tupples of Tupple<double, double, double> or a custom Vector3 Object.

			var x = v1.X * ((v2.Y * v3.Z) - (v3.Y * v2.Z));
			var y = v2.X * ((v1.Y * v3.Z) - (v3.Y * v1.Z));
			var z = v3.X * ((v1.Y * v2.Z) - (v2.Y * v1.Z));

			return x - y + z;
		}

		/// <summary>
		/// Calculate the point of intersection between a Bundle of <see cref="Autodesk.Revit.DB.Plane"/>'s. Planes must be non-parallel.
		/// </summary>
		/// <param name="firstPlane">First <see cref="Autodesk.Revit.DB.Plane"/>.</param>
		/// <param name="secondPlane">Second <see cref="Autodesk.Revit.DB.Plane"/>.</param>
		/// <param name="thirdPlane">Third <see cref="Autodesk.Revit.DB.Plane"/>.</param>
		/// <param name="pt">The Intersecting Point (if any) as <see cref="Autodesk.Revit.DB.XYZ"</param>
		/// <returns>/>Boolean if an intersection was found</returns>
		/// <remarks>Method of intersection using Cramers rule and formula taken from https://mathworld.wolfram.com/Plane-PlaneIntersection.html </remarks>
		public static bool Intersect(Plane firstPlane, Plane secondPlane, Plane thirdPlane, out XYZ pt)
		{
			// Formula taken from https://mathworld.wolfram.com/Plane-PlaneIntersection.html
			// let the intersection X = 1/det * v
			// Where the determinant det = |n1 n2 n3|
			// and the Vector is given by v = (x1.n1)(n2*n3) + (x2.n2)(n3*n1) + (x3.n3)(n1*n2)

			pt = null;

			// Plane Origins...
			var x1 = firstPlane.Origin;
			var x2 = secondPlane.Origin;
			var x3 = thirdPlane.Origin;

			// Plane Normals...
			var n1 = firstPlane.Normal.Normalize();
			var n2 = secondPlane.Normal.Normalize();
			var n3 = thirdPlane.Normal.Normalize();

			// Calculate the determinant where det = |n1 n2 n3|...
			double det = Determinant3x3(n1, n2, n3);

			// Check if determinant is almost 0, if so then no intersection could be found...
			if (Math.Abs(det) < double.Epsilon)
				return false;

			// Calculate v where v = (x1.n1)(n2*n3) + (x2.n2)(n3*n1) + (x3.n3)(n1*n2)...
			var v = (x1.DotProduct(n1) * n2.CrossProduct(n3)) + (x2.DotProduct(n2) * n3.CrossProduct(n1)) + (x3.DotProduct(n3) * n1.CrossProduct(n2));

			// Calculate intersecting Point if there is one where the point X = 1/det * v...
			pt = (1 / det) * v;

			return pt != null;
		}

		/// <summary>
		/// Intersects a sheath of Planes. That is a <see cref="Autodesk.Revit.DB.Plane"/> with another <see cref="Autodesk.Revit.DB.Plane"/>.
		/// </summary>
		/// <param name="plane">The first <see cref="Autodesk.Revit.DB.Plane"/>.</param>
		/// <param name="otherPlane">The second <see cref="Autodesk.Revit.DB.Plane"/></param>
		/// <param name="line">The intersecting <see cref="Autodesk.Revit.DB.Line"/>.</param>
		/// <returns>Boolean if intersection was found</returns>
		public static bool Intersect(this Plane plane, Plane otherPlane, out Line line)
		{
			line = null;

			// Parallel Plane Test...
			if (Math.Abs(plane.Normal.DotProduct(otherPlane.Normal)) == 1)
				return false;

			// Calculate the Vector representing the Lines direction...
			var dir = plane.Normal.CrossProduct(otherPlane.Normal);

			// Create a plane perpendicular to the other planes, that is, perpendicular to the direction of the Line. This will be used to get a point of intersection on the Line as the Origin...
			var pl3 = Plane.CreateByNormalAndOrigin(dir.Normalize(), dir);

			XYZ xPt;
			// Get a point of intersection on the Line as the Origin...
			if (!Intersect(plane, otherPlane, pl3, out xPt))
				return false;

			// Create a new Bound Line representing the intersection of the two given planes...
			line = Line.CreateBound(xPt.Add(dir.Normalize() * 1000), xPt.Subtract(dir.Normalize() * 1000));
			return line != null;
		}

		/// <summary>
		/// Calculates the intersection between the given <see cref="Autodesk.Revit.DB.Plane"/> and the given <see cref="Autodesk.Revit.DB.Line"/>.
		/// </summary>
		/// <param name="plane">The intersection <see cref="Autodesk.Revit.DB.Plane"/>.</param>
		/// <param name="line">The <see cref="Autodesk.Revit.DB.Line"/> to intersect</param>
		/// <param name="apparentIntersection">Should this calculate apparent intersection. i.e. if the plane and line do not actually intersect a point on the line is projected to Plane.</param>
		/// <returns></returns>
		public static bool Intersect(Plane plane, Line line, out XYZ pt, bool apparentIntersection = false)
		{
			pt = null;

			// Get the Direction of the Line. NOTE: We could use Line.Direction here instead...
			var lnDir = (line.GetEndPoint(1) - line.GetEndPoint(0));

			// Calculate the Numerator and Denominator, this is using the standard formula for projecting vectors...
			var numerator = (plane.Origin - line.GetEndPoint(0)).DotProduct(plane.Normal);
			var denominator = plane.Normal.DotProduct(lnDir);

			// If the Denominator or Numerator is 0, then return false...
			if (denominator == 0 || numerator == 0)
				return false;

			// Calculate the magnitude of the translation...
			var d = numerator / denominator;

			// Apply the transformation to the point at the start of the Line. X=X0 + dL...
			pt = line.GetEndPoint(0) + (lnDir * d);

			// If we only want an intersection within the bounds of the Line Segment...
			if (!apparentIntersection)
				pt = line.IsPointWithinBounds(pt) ? pt : null;

			return pt != null;
		}

		/// <summary>
		/// Is the given Point within the bounds of the given <see cref="Line"/> segment, that is, between the parameters 0 and 1 of the Line.
		/// </summary>
		/// <param name="line">The <see cref="Line"/></param>
		/// <param name="point">The Point to test</param>
		/// <returns></returns>
		public static bool IsPointWithinBounds(this Line line, XYZ point)
		{
			var a = line.GetEndPoint(0);
			var b = line.GetEndPoint(1);
			var c = point;

			// The Dot Product...
			var dp = (b - a).DotProduct(c - a);

			// If the Dot Product is less than 0, this means that the point is before the start of the Line...
			if (dp < 0)
				return false;

			// The Squared Magnitude of the vector AB..
			var sqMag = (b - a).DotProduct(b - a);

			// If the Dot Product is greater than the Squared Magnitude, then this point is beyond the end of the Line...
			if (dp > sqMag)
				return false;

			return true;
		}

		/// <summary>
		/// Ensures the given Point is within the bounds of the given <see cref="Line"/> segment, that is, between the parameters 0 and 1 of the Line. Closest Endpoint method is used of the Point falls outside the bounds.
		/// </summary>
		/// <param name="line">The <see cref="Line"/></param>
		/// <param name="point">The Point to operate on</param>
		/// <returns></returns>
		public static XYZ EnsurePointWithinBounds(this Line line, XYZ point)
		{
			// If the Point is NOT within the bounds, then return the closest endpoint to the point...
			if (!line.IsPointWithinBounds(point))
			{
				var a = line.GetEndPoint(0);
				var b = line.GetEndPoint(1);

				return a.DistanceTo(point) < b.DistanceTo(point) ? a : b;
			}

			return point;
		}

		/// <summary>
		/// Projects the given Point onto the given <see cref="Line"/>.
		/// </summary>
		/// <param name="point">The Point to project</param>
		/// <param name="line">The <see cref="Line"/> to project onto</param>
		/// <param name="withinBoundsOnly">Ensure this point is projected within the bounds of the <see cref="Line"/>. If projected Point lies outside of the Line segment, then nearest endpoint will be used.</param>
		/// <returns></returns>
		public static XYZ ProjectPointOnto(this XYZ point, Line line, bool withinBoundsOnly = true)
		{
			// The Vector between a point X0 on the Line and the Point to Project...
			var p = point - line.GetEndPoint(0);

			// The Vector Representing the Line. This is essentially the Direction of the Line with magitude equal to length of the Line...
			var q = line.GetEndPoint(1) - line.GetEndPoint(0);

			// Vector representing the translation of the Point X0 on the Line to the projected Point perpendicular to the Line...
			var perpP = p - (p.DotProduct(q) / q.DotProduct(q)) * q;

			// The Projected Point on the Line...
			var projPt = point - perpP;

			// If we want to keep the projected Point within the bounds of the Line Segment (closest endpoint method is used here)...
			if (withinBoundsOnly)
				return line.IsPointWithinBounds(projPt) ? projPt : line.EnsurePointWithinBounds(projPt);

			return projPt;
		}

		/// <summary>
		/// Test if the given Point is on the given <see cref="Line"/>.
		/// </summary>
		/// <param name="point">The Point to test</param>
		/// <param name="line">The <see cref="Line"/> to test with</param>
		/// <param name="withinBoundsOnly">If the Point Test should lie within the bounds of the <see cref="Line"/> segment, that is, between the Parameters 0 and 1 of the Line.</param>
		/// <returns></returns>
		public static bool IsOnLine(this XYZ point, Line line, bool withinBoundsOnly = true)
		{
			// Test if Point SHOULD be within bounds and it is NOT within bounds...
			if (withinBoundsOnly && !line.IsPointWithinBounds(point))
				return false;

			// Get normalized directional vectors...
			var p = point - line.GetEndPoint(0).Normalize();
			var q = (line.GetEndPoint(1) - line.GetEndPoint(0)).Normalize();

			// If absolute value of Dot Product is 1, then the Point is on the Line, otherwise it is not...
			return Math.Abs(p.DotProduct(q)) == 1 ? true : false;
		}
	}
}
