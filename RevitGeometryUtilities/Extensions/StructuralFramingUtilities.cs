﻿using System;
using System.Collections.Generic;
using System.Linq;

using Autodesk.Revit.DB;
using Autodesk.Revit.DB.Structure.StructuralSections;

using RevitGeometryUtilities.Enums;
using RevitGeometryUtilities.Extensions;
using RevitGeometryUtilities.Geometry;

namespace RevitGeometryUtilities.Extensions
{
    public static class StructuralFramingUtilities
    {
		/// <summary>
		/// Relocates the LocationCurve to other location.
		/// </summary>
		/// <param name="doc"></param>
		/// <param name="fi"></param>
		/// <param name="pos"></param>
		/// <returns></returns>
		public static Line RelocateLocationCurve(FamilyInstance fi, StickPositionEnum pos = StickPositionEnum.UpperCentre)
		{
			// TODO: Perhaps allow for Vertical Columns here by Calculating Vertical Line based on Centre of Column, this will be Driving Curve. Get profile and rotate by it's facing direction and move to correct location.

			var doc = fi.Document;

			var fs = fi.Symbol;
			var fm = fs.Family;

			if (fm.FamilyPlacementType != FamilyPlacementType.CurveDrivenStructural)
				throw new Exception("This Family is not suitable for operating on.");

			if (!fm.CanHaveStructuralSection())
				throw new Exception("This Family is not suitable for operating on.");

			var sProf = fi.GetSweptProfile();
			var dCrv = sProf.GetDrivingCurve();

			if (dCrv.GetType() != typeof(Line))
				throw new Exception("This method can only operate on linear Elements");

			var plane = ((Line)dCrv).GetAlignedPlane();

			var rot = fi.get_Parameter(BuiltInParameter.STRUCTURAL_BEND_DIR_ANGLE).AsDouble();

			var spt = GetTransformedPosition(sProf.GetSweptProfile(), plane, pos, rot);
			var ept = spt.Add(dCrv.GetEndPoint(1) - dCrv.GetEndPoint(0));

			return Line.CreateBound(spt, ept);
		}

		/// <summary>
		/// Gets the Stick Position from profile and transforms to Plane.
		/// </summary>
		/// <param name="profile">The Profile to analyse</param>
		/// <param name="plane">The Plane to Transform to</param>
		/// <param name="pos">The Position on the Profile to return</param>
		/// <param name="rotation">Optional Rotation in Radians. If you are getting position of beam that has cross-section rotation, then use this value.</param>
		/// <returns>Point that represents the the location of the given position</returns>
		private static XYZ GetTransformedPosition(this Profile profile, Plane plane, StickPositionEnum pos, double rotation = 0)
        {
			var bbox = profile.GetBoundingBox();

			var bboxCtr = bbox.Min.Add(bbox.Max).Divide(2);

			// Profile may not have origin at 0,0,0, so we get delta here...
			var d = XYZ.Zero.Subtract(bboxCtr);
			var dx = plane.XVec.Normalize().Multiply(d.X);
			var dy = plane.YVec.Normalize().Multiply(d.Y);

			// Get plan width/height of BoundingBox...
			var v = bbox.Max.Subtract(bbox.Min);
			var x = plane.XVec.Normalize().Multiply(v.X / 2);
			var y = plane.YVec.Normalize().Multiply(v.Y / 2);

			// Set the Origin with respective profile delta...
			var ctr = plane.Origin.Add(dx).Add(dy);
			var pt = ctr;

			if (pos == StickPositionEnum.UpperLeft)
			{
				pt = ctr.Subtract(x).Add(y);
			}
			if (pos == StickPositionEnum.UpperCentre)
			{
				pt = ctr.Add(y);
			}
			if (pos == StickPositionEnum.UpperRight)
			{
				pt = ctr.Add(x).Add(y);
			}
			if (pos == StickPositionEnum.MidLeft)
			{
				pt = ctr.Subtract(x);
			}
			if (pos == StickPositionEnum.MidRight)
			{
				pt = ctr.Add(x);
			}
			if (pos == StickPositionEnum.LowerLeft)
			{
				pt = ctr.Subtract(x).Subtract(y);
			}
			if (pos == StickPositionEnum.LowerCentre)
			{
				pt = ctr.Subtract(y);
			}
			if (pos == StickPositionEnum.LowerRight)
			{
				pt = ctr.Add(x).Subtract(y);
			}

			// If a rotation is given, then rotate the point in the plane...
			if (rotation != 0)
            {
				var t = Transform.CreateRotationAtPoint(plane.Normal, rotation, plane.Origin);
				pt = t.OfPoint(pt);
            }

			return pt;
		}
	}
}
