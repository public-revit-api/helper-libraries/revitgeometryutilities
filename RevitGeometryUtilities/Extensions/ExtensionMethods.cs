﻿using System;
using System.Collections.Generic;
using System.Linq;

using Autodesk.Revit.DB;
using Autodesk.Revit.UI;

using RevitGeometryUtilities.Geometry;

namespace RevitGeometryUtilities.Extensions
{
	/// <summary>
	/// Collection of Revit Type helper Methods.
	/// </summary>
    public static class ExtensionMethods
    {

		/// <summary>
		/// Convert the Level to a Plane.
		/// </summary>
		/// <param name="lvl">The Level to convert</param>
		/// <returns>Plane</returns>
		public static Plane AsPlane(this Level lvl)
		{
			var doc = lvl.Document;
			Plane plane;

			var lvlType = (LevelType)doc.GetElement(lvl.GetTypeId());

			// Check Elevation Base of Level so we can get correct Elevation value... We Want the global value, not the augmented "project" value...
			if (lvlType.get_Parameter(BuiltInParameter.LEVEL_RELATIVE_BASE_TYPE).AsInteger() == 0)
			{
				var pbp = new FilteredElementCollector(doc).OfCategory(BuiltInCategory.OST_ProjectBasePoint).FirstOrDefault();

				var pbpElev = pbp.get_Parameter(BuiltInParameter.BASEPOINT_ELEVATION_PARAM).AsDouble();
				plane = Plane.CreateByNormalAndOrigin(XYZ.BasisZ, new XYZ(0, 0, lvl.Elevation + pbpElev));
			}
			else
			{
				plane = Plane.CreateByNormalAndOrigin(XYZ.BasisZ, new XYZ(0, 0, lvl.Elevation));
			}

			return plane;
		}

		/// <summary>
		/// Convert this Grid to a Plane.
		/// </summary>
		/// <param name="grid">The Grid to convert</param>
		/// <returns>Plane</returns>
		public static Plane AsPlane(this Grid grid)
		{
			var crv = grid.Curve;

			if (crv is Line)
			{
				crv = (Line)crv;

				var orig = crv.Evaluate(0.5, true);
				var norm = crv.GetEndPoint(1).Subtract(crv.GetEndPoint(0)).Normalize().CrossProduct(XYZ.BasisZ);

				return Plane.CreateByNormalAndOrigin(norm, orig);
			}

			return null;
		}

		/// <summary>
		/// Convert the ReferencePlane to a Plane.
		/// </summary>
		/// <param name="refPlane">The ReferencePlane to convert</param>
		/// <returns>Plane</returns>
		public static Plane AsPlane(this ReferencePlane refPlane)
		{
			return refPlane.GetPlane();
		}

		/// <summary>
		/// Converts the Face to a Plane at the given Reference.
		/// </summary>
		/// <param name="face">The Face</param>
		/// <param name="doc">The Document the Face is in</param>
		/// <param name="reference">The Reference to where the face is evaluated</param>
		/// <returns>Plane</returns>
		public static Plane AsPlane(this Face face, Document doc, Reference reference)
		{
            XYZ origin, normal;

			var element = doc.GetElement(reference.ElementId);

			var uv = reference.UVPoint == null ? new UV(0.5, 0.5) : reference.UVPoint;

            // When an Elements Geometry has been modified, Revit now treats the Geometry as In-Place. This affects the Normal/Origin when evaluating and needs to be transformed into global space.
            // See https://thebuildingcoder.typepad.com/blog/2017/06/picked-family-instance-face-geometry-in-lcs-versus-wcs.html
            if (reference.ConvertToStableRepresentation(doc).Contains("INSTANCE"))
            {
                // Get the Transform of the Family Instance...
                Transform t = ((FamilyInstance)element).GetTransform();

                // Transform the XYZ's to the actual position of the Family Instance...
                normal = t.OfVector(face.ComputeNormal(uv));
                origin = t.OfPoint(face.Evaluate(uv));
            }
            else
            {
                origin = face.Evaluate(uv);
                normal = face.ComputeNormal(uv).Normalize();
            }

            return Plane.CreateByNormalAndOrigin(normal, origin);
		}

		/// <summary>
		/// Gets the perpendicular Plane at the given Parameter along a line.
		/// </summary>
		/// <param name="ln">The Line to operate on</param>
		/// <param name="t">The parameter between 0 & 1 to create Plane</param>
		/// <returns>Plane</returns>
		public static Plane GetPlaneAtParameter(this Line ln, double t)
		{
			// Ensure value is between 0 & 1...
			if (t > 1)
				t = 1;

			if (t < 0)
				t = 0;

			var normal = (ln.GetEndPoint(1) - ln.GetEndPoint(0)).Normalize();
			var origin = ln.Evaluate(t, true);

			return Plane.CreateByNormalAndOrigin(normal, origin);
		}

		/// <summary>
		/// Gets a Vertical Plane that passes through the given Line.
		/// </summary>
		/// <param name="ln">The Line to operate on</param>
		/// <returns>Plane</returns>
		public static Plane GetVerticalPlane(this Line ln)
		{
			// Get origin at midpoint of the Line...
			var origin = ln.Evaluate(0.5, true);

			// If Line is vertical then return a vertical plane with normal facing BasisX...
			if (ln.IsVertical())
				return Plane.CreateByNormalAndOrigin(XYZ.BasisX, origin);

			var dir = (ln.GetEndPoint(1) - ln.GetEndPoint(0)).Normalize();

			// Calculate Normal as Cross Product between Line Direction and global ZAxis...
			var normal = dir.CrossProduct(XYZ.BasisZ).Normalize();

			//TODO: Perhaps use Plane.ByOriginAndBasis so axes are aligned to Line with respect to global global Z-Axis...			
			return Plane.CreateByNormalAndOrigin(normal, origin);
		}

		/// <summary>
		/// Gets a plane aligned to the Lines direction.
		/// </summary>
		/// <param name="ln"></param>
		/// <param name="t">(Optional) The Parameter on the Line between 0 and 1. Default value of 0.</param>
		/// <returns>Aligned Plane</returns>
		public static Plane GetAlignedPlane(this Line ln, double t = 0)
        {
			if (t > 1)
				t = 1;

			if (t < 0)
				t = 0;

			var orig = ln.Evaluate(t, true);
			var n = (ln.GetEndPoint(1) - ln.GetEndPoint(0)).Normalize();
			var bx = n.CrossProduct(XYZ.BasisZ).Normalize();
			var by = n.CrossProduct(bx).Normalize().Negate();

			return Plane.CreateByOriginAndBasis(orig, bx, by);
        }

		/// <summary>
		/// Gets the Plane that passes horizontally through the given Line.
		/// </summary>
		/// <param name="ln"></param>
		/// <returns>Plane</returns>
		public static Plane GetHorizontalPlane(this Line ln)
		{
			var origin = ln.Evaluate(0.5, true);

			// If the Line is vertical then create a plane facing up through the midpoint of the line...
			if (ln.IsVertical())
				return Plane.CreateByNormalAndOrigin(XYZ.BasisZ, origin);

			var dir = (ln.GetEndPoint(1) - ln.GetEndPoint(0)).Normalize();

			var v = dir.CrossProduct(XYZ.BasisZ);
			var normal = dir.CrossProduct(v);

			return Plane.CreateByNormalAndOrigin(normal, origin);
		}

		/// <summary>
		/// Get Basis Planes from a Point. That is, the 3 Planes that align to X, Y, Z basis vectors with this point as the Origin of the planes.
		/// </summary>
		/// <param name="pt">The Origin Point</param>
		/// <returns>Plane Array</returns>
		public static Plane[] BasisPlanes(this XYZ pt)
		{
			return new Plane[]
			{
				Plane.CreateByNormalAndOrigin(XYZ.BasisX, pt),
				Plane.CreateByNormalAndOrigin(XYZ.BasisY, pt),
				Plane.CreateByNormalAndOrigin(XYZ.BasisZ, pt)
			};
		}

		/// <summary>
		/// Get the Min and Max point as a bundle of Basis Planes. 
		/// </summary>
		/// <param name="bbox"></param>
		/// <returns>List of Plane Array. 2No. 3xPlanes</Plane[]></returns>
		public static List<Plane[]> ToPlanes(this BoundingBoxXYZ bbox)
		{
			var planes = new List<Plane[]>();

			planes.Add(bbox.Min.BasisPlanes());
			planes.Add(bbox.Max.BasisPlanes());

			return planes;
		}

		/// <summary>
		/// Get the MidPoint between this Point and another.
		/// </summary>
		/// <param name="pt"></param>
		/// <param name="otherPt"></param>
		/// <returns>Point as XYZ</returns>
		public static XYZ MidPoint(this XYZ pt, XYZ otherPt)
		{
			return pt.Add(otherPt).Divide(2);
		}

		/// <summary>
		/// Gets the Midpoint of the given list of Points.
		/// </summary>
		/// <param name="points">List of XYZ</param>
		/// <returns>Point as XYZ</returns>
		public static XYZ MidPoint(List<XYZ> points)
		{
			// Check if input arg is valid...
			if (points == null || points.Count < 2)
				return null;

			// Get the first point...
			var pt = points.FirstOrDefault();

			// Add all the points...
			for (int i = 1; i < points.Count; i++)
			{
				pt.Add(points[i]);
			}

			// Divide the components of the point by the number of points to get the MidPoint...
			return pt.Divide(points.Count);
		}

		/// <summary>
		/// Bounds the given Line to the extents of the given BoundingBox.
		/// </summary>
		/// <param name="ln">The Line to operate on</param>
		/// <param name="bbox">The BoundingBox which represents the extents of the Line</param>
		/// <returns>Bound Line</returns>
		public static Line BoundToModelExtents(this Line ln, BoundingBoxXYZ bbox)
		{
			//TODO: We should test if projected midpoint is actually within the bounding box.
			// Perhaps, we should intersect the line through a plane at the centre of the box instead of projecting as this will ensure that the point is within extents of Bbox. NEEDS MORE THOUGHT!!!

			// Project midpoint of BoundingBox onto Unbound Line...
			var pt = bbox.Min.MidPoint(bbox.Max).ProjectPointOnto(ln, false);

			// Get the 6 basis planes of the BoundingBox. This works as BoundingBoxes are orthogonal to global coordinate system...
			var planes = bbox.ToPlanes();

			// Get Intersections of unbound line and planes...
			//List<XYZ> minXpts = planes[0].ToList().Select(x => GeometryUtilities.Intersect(x, ln, true)).ToList();
			//List<XYZ> maxXpts = planes[1].ToList().Select(x => GeometryUtilities.Intersect(x, ln, true)).ToList();
			var minXpts = new List<XYZ>();
			foreach (var p in planes[0])
            {
				XYZ X;
				if (GeometryUtilities.Intersect(p, ln, out X, true))
					minXpts.Add(X);
            }

			var maxXpts = new List<XYZ>();
			foreach (var p in planes[1])
			{
				XYZ X;
				if (GeometryUtilities.Intersect(p, ln, out X, true))
					maxXpts.Add(X);
			}

			// Sort intersections by distance to projected midpoint. NOTE: This may not work if projected midpoint is outside of BoundingBox extents...
			// TODO: If there are more than one point in the min/max point lists, then test which combination is within bounds, we want the points that sit on the face of the bounding box.
			// In some cases though, the whole line might sit outside the boundingbox, this should be handled if the Line is far away from the model,
			// a tolerance should be used so if the Line is more than X units away, then even though there is an intersection, this method will return null.
			var xPt1 = minXpts.Where(x => x != null).OrderBy(x => x.DistanceTo(pt)).FirstOrDefault();
			var xPt2 = maxXpts.Where(x => x != null).OrderBy(x => x.DistanceTo(pt)).FirstOrDefault();

			if (xPt1 == null || xPt2 == null)
				return null;

			// Create the new Line...
			return Line.CreateBound(xPt1, xPt2);
		}

		/// <summary>
		/// Test if Line is Vertical.
		/// </summary>
		/// <param name="ln">The Line to Test</param>
		/// <returns></returns>
		public static bool IsVertical(this Line ln)
		{
			var dir = (ln.GetEndPoint(1) - ln.GetEndPoint(0)).Normalize();

			// Test if Dot Product of Line direction and Z-Axis is almost equal to 1...
			return (1 - Math.Abs(dir.DotProduct(XYZ.BasisZ))) <= double.Epsilon;
		}

		/// <summary>
		/// Test if the Line is Horizontal.
		/// </summary>
		/// <param name="ln">The Line to Test</param>
		/// <returns></returns>
		public static bool IsHorizontal(this Line ln)
		{
			var dir = (ln.GetEndPoint(1) - ln.GetEndPoint(0)).Normalize();

			// Test if Dot Product between Line direction and Z-Axis is almost 0...
			return Math.Abs(dir.DotProduct(XYZ.BasisZ)) <= double.Epsilon;
		}

		/// <summary>
		/// Test is the Line is Raking.
		/// </summary>
		/// <param name="ln">The Line to Test</param>
		/// <returns></returns>
		public static bool IsRaking(this Line ln)
		{
			return !ln.IsHorizontal() && !ln.IsVertical();
		}

		/// <summary>
		/// Gets the sum of all bounding boxes as one BoundingBox that represents the Extents of the model or of the given Elements if there are any.
		/// </summary>
		/// <param name="doc">The Document to calculate Extents of</param>
		/// <param name="paddingAmount">The amount of padding to add/subtract. Allows for all real values in domain of |R. Default value of 0.</param>
		/// <param name="elems">Optional List of Elements to operate on. If given, then the BoundingBox will encapsulate the BoundingBoxes of only these Elements.</param>
		/// <remarks>This method has been modified from a sample taken from The Building Coder. This only considers Elements where the Element Category is Cuttable. Credit to Jeremy Tammik. https://jeremytammik.github.io/tbc/a/1456_bounding_section_box.html</remarks>
		/// <returns><see cref="BoundingBoxXYZ"/> encapsulating all BoundingBoxes</returns>
		public static BoundingBoxXYZ GetExtents(this Document doc, double paddingAmount = 0, List<Element> elems = null)
		{
			// If the optional argument elems is null, then lets use all Elements in the Model that are "Cuttable"...
			if (elems == null)
				elems = new FilteredElementCollector(doc).WhereElementIsNotElementType()
														 .WhereElementIsViewIndependent()
														 .Where(e => e.Category != null && e.Category.IsCuttable)
														 .ToList();
			else
				// Ensure we have only "Cuttable" Elements...
				// TODO: This needs rigorous testing as there are categories like Stairs that don't allow cutting and won't be considered.
				elems = elems.Where(e => e.Category.IsCuttable).ToList();

			// Slight modification to ensure that there is a bbox...
			IEnumerable<BoundingBoxXYZ> bbs = elems.Where(e => e.get_BoundingBox(null) != null).Select<Element, BoundingBoxXYZ>(e => e.get_BoundingBox(null));

			// Expand BoundingBox to encapsulate all BoundingBoxes that are outside the BoundingBox...
			var bbox = bbs.Aggregate<BoundingBoxXYZ>((a, b) => { a.ExpandToContain(b); return a; });

			// If padding amount is not the default value of 0, then apply padding...
			if (paddingAmount != 0)
            {
				// Create vector from paddingAmount...
				var padding = new XYZ(paddingAmount, paddingAmount, paddingAmount);

				// Apply Padding...
				bbox.Min = bbox.Min - padding;
				bbox.Max = bbox.Max + padding;
			}

			return bbox;
		}

		/// <summary>
		/// Expand the given bounding box to include.
		/// Credit to Jeremy Tammik. https://jeremytammik.github.io/tbc/a/1456_bounding_section_box.html
		/// and contain the given point.
		/// </summary>
		public static void ExpandToContain(this BoundingBoxXYZ bb, XYZ p)
		{
			bb.Min = new XYZ(Math.Min(bb.Min.X, p.X), Math.Min(bb.Min.Y, p.Y), Math.Min(bb.Min.Z, p.Z));
			bb.Max = new XYZ(Math.Max(bb.Max.X, p.X), Math.Max(bb.Max.Y, p.Y), Math.Max(bb.Max.Z, p.Z));
		}

		/// <summary>
		/// Expand the given bounding box to include.
		/// Credit to Jeremy Tammik. https://jeremytammik.github.io/tbc/a/1456_bounding_section_box.html
		/// and contain the given other one.
		/// </summary>
		public static void ExpandToContain(this BoundingBoxXYZ bb, BoundingBoxXYZ other)
		{
			bb.ExpandToContain(other.Min);
			bb.ExpandToContain(other.Max);
		}

		/// <summary>
		/// Gets the closest points between this Line and another.
		/// </summary>
		/// <param name="lnA">First Line</param>
		/// <param name="lnB">Second Line</param>
		/// <param name="keepWithinBounds">Should the point remain within the bounds of the Line segments, or should the points be the projected closest point.</param>
		/// <returns></returns>
		public static List<XYZ> GetClosestPoints(this Line lnA, Line lnB, bool keepWithinBounds = true)
		{
			// TODO: Try make similar to Revit API version where keep within bounds can be set of both lines...
			// TODO: There is a bug where if keepWithinBounds is set, of Lines are parallel, then closest point can result at opposite endpoints and of course is not the closest point.
			// Instead, we should first test if lines overlap (they don't need to be colinear, but where L1.Length + L2.Length > the projected line onto the other lines Length). Then we should get closest Points.
			// If the lines "Overlap" then we should project the point that is within the combined segments, otherwise nearest endpoints method should be used. 			

			var pts = new List<XYZ>();

			var u = lnA.GetEndPoint(1) - lnA.GetEndPoint(0);
			var v = lnB.GetEndPoint(1) - lnB.GetEndPoint(0);
			var w = lnA.GetEndPoint(0) - lnB.GetEndPoint(0);

			double a = u.DotProduct(u);
			double b = u.DotProduct(v);
			double c = v.DotProduct(v);
			double d = u.DotProduct(w);
			double e = v.DotProduct(w);

			var denominator = a * c - b * b;

			// Scalar variable...
			double s = 0;
			double t = 0;

			if (denominator == 0)
			{
				t = d / b;
			}
			else
			{
				s = (b * e - c * d) / denominator;
				t = (a * e - b * d) / denominator;
			}

			XYZ p1 = lnA.GetEndPoint(0) + s * u;
			XYZ p2 = lnB.GetEndPoint(0) + t * v;

			if (keepWithinBounds)
			{
				if (!lnA.IsPointWithinBounds(p1))
				{
					pts.Add(lnA.EnsurePointWithinBounds(p1));
					pts.Add(p2.ProjectPointOnto(lnB));
					return pts;
				}

				if (!lnB.IsPointWithinBounds(p2))
				{
					pts.Add(lnB.EnsurePointWithinBounds(p2));
					pts.Add(p1.ProjectPointOnto(lnA));
					return pts;
				}
			}

			pts.Add(p1);
			pts.Add(p2);
			return pts;
		}

		/// <summary>
		/// Creates a new <see cref="ReferencePlane"/> from the given <see cref="Plane"/> in the given <see cref="UIDocument"/>.
		/// </summary>
		/// <param name="plane">The <see cref="Plane"/> that represents the base geometry for the <see cref="ReferencePlane"/></param>
		/// <param name="uidoc">The <see cref="UIDocument"/> to operate on.</param>
		/// <param name="showPlaneInView">Should the <see cref="ReferencePlane"/> be shown in the Active View. The Views active <see cref="SketchPlane"/> will be set to the new <see cref="ReferencePlane"/>.</param>
		/// <param name="name">The user visible name of the <see cref="ReferencePlane"/>. If a <see cref="ReferencePlane"/> already exists with this name, then this <see cref="ReferencePlane"/> will be deleted and a new one created</param>
		/// <returns>The newly created <see cref="ReferencePlane"/></returns>
		public static ReferencePlane ToReferencePlane(this Plane plane, UIDocument uidoc, bool showPlaneInView = false, string name = "Temporary Working Plane")
		{
			var doc = uidoc.Document;

			ReferencePlane rp;

			// Create ref plane ends...
			var bblEnd = plane.Origin.Add(plane.XVec.Normalize());
			var freeEnd = plane.Origin.Subtract(plane.XVec.Normalize());

			using (Transaction t = new Transaction(doc, "Create Reference Plane"))
			{
				t.Start();

				try
				{
					// Get old RefPlane if one exists already...
					var oldRp = new FilteredElementCollector(doc)
									.OfClass(typeof(ReferencePlane))
									.WhereElementIsNotElementType()
									.Where(r => r.Name == name)
									.FirstOrDefault();

					// If old RefPlane exists, then delete it...
					if (oldRp != null)
					{
						doc.Delete(oldRp.Id);
					}

					// Create new Ref Plane and set Name...
					rp = doc.Create.NewReferencePlane(bblEnd, freeEnd, plane.YVec.Normalize(), doc.ActiveView);
					rp.Name = name;

					// If we should set the Active Views Reference Plane as the Active SketchPlane...
					if (showPlaneInView)
					{
						uidoc.ActiveView.SketchPlane = SketchPlane.Create(doc, rp.Id);
						uidoc.ActiveView.ShowActiveWorkPlane();
					}
				}
				catch (Exception ex)
				{
					TaskDialog.Show("Error", "Something went wrong and operation will be cancelled... \n\n" + ex.Message);
					t.RollBack();
					return null;
				}
				t.Commit();
			}

			return rp;
		}

		/// <summary>
		/// Check if Curve is Linear.
		/// </summary>
		/// <param name="crv">The Curve to test</param>
		/// <remarks>Uses length test to determine if vector magnitude between endpoints is almost equal to the length of the Curve.</remarks>
		/// <returns>True is the Curve is Linear. False otherwise.</returns>
		public static bool IsLinear(this Curve crv)
        {
			return crv.ApproximateLength - crv.GetEndPoint(0).DistanceTo(crv.GetEndPoint(1)) <= double.Epsilon;
        }

		/// <summary>
		/// Get <see cref="BoundingBoxXYZ"/> that circimscribes the given <see cref="Profile"/>.
		/// </summary>
		/// <param name="profile">The Profile to get BoundingBox</param>
		/// <returns><see cref="BoundingBoxXYZ"/></returns>
		public static BoundingBoxXYZ GetBoundingBox(this Profile profile)
        {
			var crvs = profile.Curves;

			if (crvs.IsEmpty)
				return null;

			double minX = 0;
			double minY = 0;
			double maxX = 0;
			double maxY = 0;

            foreach (Curve c in crvs)
            {
				// If Curve is Linear, then we only care about the origin of the Line. Since we are dealing with a profile we can assume all curves are connected and just one endpoint is sufficient...
				if (c.IsLinear())
                {
					var pt = c.GetEndPoint(0);

					if (pt.X < minX) minX = pt.X;
					if (pt.Y < minY) minY = pt.Y;
					if (pt.X > maxX) maxX = pt.X;
					if (pt.Y > maxY) maxY = pt.Y;
				}
				// If Curve is NOT Linear, then get points along curve. I am relying on profile curves mostly being arcs and/or relatively short so shouldn't be overly expensive.
				// There is probably a better way to do this mathematically or by algorithm solving for min/max value of x/y, but this would probably be far more verbose and likely more computationaly expensive, but would be more accurate.
				// Something to look into if this method proves sufficiently inaccurate. Good place to start might be in Realtime Collision Detection [BOOK] for determining bounding boxes or in Mathematics for 3D programming and Computer Graphics.
                else
                {
					int div = 10;
					for (int i = 0; i <= div; i++)
					{
						double t = 1.0 / div * i;
						var pt = c.Evaluate(t, true);

						if (pt.X < minX) minX = pt.X;
						if (pt.Y < minY) minY = pt.Y;
						if (pt.X > maxX) maxX = pt.X;
						if (pt.Y > maxY) maxY = pt.Y;
					}
                }
			}

			var bbox = new BoundingBoxXYZ();
			bbox.Min = new XYZ(minX, minY, 0);
			bbox.Max = new XYZ(maxX, maxY, 0);

			return bbox;
        }
	}
}
