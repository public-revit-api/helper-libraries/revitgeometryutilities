﻿namespace RevitGeometryUtilities.Enums
{
    public enum StickPositionEnum
    {
		UpperLeft = 0,
		UpperCentre = 1,
		UpperRight = 2,
		MidLeft = 3,
		MidCentre = 4,
		MidRight = 5,
		LowerLeft = 6,
		LowerCentre = 7,
		LowerRight = 8
	}
}
